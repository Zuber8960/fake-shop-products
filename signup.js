const form = document.querySelector("form");
// console.log(form);
const checkBox = document.getElementById("checkbox");
const firstName = document.getElementById("firstName");
const lastName = document.getElementById("lastName");
const email = document.getElementById("email");


const password = document.getElementById("password");
const confirmPassword = document.getElementById("confirmPassword");

const errorMessage = document.getElementById("error-message");
const successMessage = document.getElementById("success");
// console.log(errorMessage);

// checkBox.onclick(() => {
// })

form.addEventListener("submit", (event) => {
    event.preventDefault();
    // console.log(errorMessage.childNodes);
    
    if(errorMessage.childNodes.length){
        console.log(errorMessage.childNodes.length);
        errorMessage.removeChild(errorMessage.children[0]);
    }else if(successMessage.childNodes.length){
        successMessage.removeChild(successMessage.children[0]);
    }
    let message;
    let flag = true;
    if (!firstName.value && !lastName.value && !email.value && !password.value && !confirmPassword.value) {
        flag = false;
        message = "Please fill All Feilds";
    } else if (!firstName.value) {
        flag = false;
        message = "Please Enter First Name";
    } else if (checkName(firstName.value)) {
        flag = false;
        message = "First Name contains only letters";
    } else if (!lastName.value) {
        flag = false;
        message = "Please Enter Last Name";
    } else if (checkName(lastName.value)) {
        flag = false;
        message = "Last Name contains only letters";
    } else if (!email.value) {
        flag = false;
        message = "Please Enter Email";
    } else if (emailCheck(email.value)) {
        flag = false;
        message = "Email is Invalid. Email format is incorrect";
    } else if (!password.value) {
        flag = false;
        message = "Please Enter Password.";
    } else if (!confirmPassword.value) {
        flag = false;
        message = "Please Enter Password Again.";
    } else if (password.value !== confirmPassword.value) {
        flag = false;
        message = "Password not matched."
    } else if (checkBox.checked == false) {
        flag = false;
        message = "Please agreed our Terms and Conditon.";
    }

    if (flag) {
        const pTag = document.createElement("p");
        pTag.innerText = "Congratulations 👍 ! Signed up Successfully.";
        successMessage.appendChild(pTag);
        firstName.value = null;
        lastName.value = null;
        email.value = null;
        password.value = null;
        confirmPassword.value = null;
        checkBox.checked = false;
    } else {
        const pTag = document.createElement("p");
        pTag.innerText = message;
        errorMessage.appendChild(pTag);
    }


})


function checkName(name) {
    if (name.match(/[^a-zA-Z]/)) {
        return true;
    }
}



function emailCheck(email) {
    if (email.includes("@") === false) {
        return true;
    }
}
