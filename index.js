const itemList = document.getElementById("item-list");
const search = document.getElementById("search");


const electronics = document.getElementById("electronics");
const jewelery = document.getElementById("jewelery");
const mensClothing = document.getElementById("men-clothing");
const womenClothing = document.getElementById("women-clothing");

const loader = document.querySelector(".loader");
const container = document.querySelector(".main-container");
container.style.display = "none";

const categaries = [electronics, jewelery, mensClothing, womenClothing];


window.addEventListener("DOMContentLoaded", () => {

    fetch("https://fakestoreapi.com/products")
        .then(res => {
            loader.style.display = "none";
            container.style.display = "block";
            return res.json();
        })
        .then(json => {
            for (let product of json) {
                addedContentInList(product, itemList);
            };
        })
        .catch(err => {
            console.error(err);
            return window.location.href = "./error.html";
        })


    for (let categary of categaries) {
        if (categary.id == 'electronics') {
            findProductsCategaryAndAddToCategay("electronics", categary);
        } else if (categary.id == 'jewelery') {
            findProductsCategaryAndAddToCategay("jewelery", categary);
        } else if (categary.id == "men-clothing") {
            findProductsCategaryAndAddToCategay("men's clothing", categary);
        } else if (categary.id == "women-clothing") {
            findProductsCategaryAndAddToCategay("women's clothing", categary);
        }
    }
});




function addedContentInList(product, ul) {
    const liTag = document.createElement("li");

    const titleTag = document.createElement("p");
    titleTag.className = "title";
    titleTag.textContent = product.title;
    liTag.appendChild(titleTag);

    const imageTag = document.createElement("img");
    imageTag.className = "image";
    imageTag.src = product.image;
    liTag.appendChild(imageTag);

    const infoDiv = document.createElement("div");

    const priceTag = document.createElement("p");
    priceTag.className = "price";
    priceTag.innerText = `$ ${product.price}`;
    infoDiv.appendChild(priceTag);

    const ratingTag = document.createElement("p");
    ratingTag.className = "rating";
    ratingTag.innerText = product.rating.rate;
    const ratingIcon = document.createElement("i");
    ratingIcon.className = "fa-solid fa-star";
    ratingTag.appendChild(ratingIcon);
    infoDiv.appendChild(ratingTag);

    liTag.appendChild(infoDiv);

    const descriptionTag = document.createElement("p");
    descriptionTag.className = "description";
    descriptionTag.textContent = product.description;
    liTag.appendChild(descriptionTag);

    const buttonsDiv = document.createElement("div");

    liTag.appendChild(buttonsDiv);

    ul.appendChild(liTag);
};

search.addEventListener("change", () => {
    let categary = search.value;

    itemList.parentElement.style.display = "none";
    electronics.parentElement.style.display = "none";
    jewelery.parentElement.style.display = "none";
    mensClothing.parentElement.style.display = "none";
    womenClothing.parentElement.style.display = "none";

    if (categary == 'electronics') {
        electronics.parentElement.style.display = "block";
    } else if (categary == 'jewelery') {
        jewelery.parentElement.style.display = "block";
    } else if (categary == "men-clothing") {
        mensClothing.parentElement.style.display = "block";
    } else if (categary == "women-clothing") {
        womenClothing.parentElement.style.display = "block";
    } else {
        itemList.parentElement.style.display = "block";
    };
});


function findProductsCategaryAndAddToCategay(categary, container) {
    fetch(`https://fakestoreapi.com/products/category/${categary}`)
        .then(res => {
            return res.json();
        })
        .then(json => {
            for (let product of json) {
                addedContentInList(product, container);
            }
            container.parentElement.style.display = "none";
        })
        .catch((err) => {
            console.error(err);
            return window.location.href = "./error.html";
        })
};